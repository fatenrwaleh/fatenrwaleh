define(['Libraries/moment','Libraries/tostr/tostr'],function(moment,tostr) {
     
        var me = this;

        me.showValidationErrror = function(cmp, message) {
            $(cmp).closest('.form-group').addClass('has-error');
            $(cmp).siblings('.error-message').text(message).show();
            tostr.options = {
                "positionClass": "toast-bottom-right",
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000
            };
            tostr.error(message);
        }
        me.validateComponent = function(cmp, validationRules) {
            var result = true;
            var message = '';
            var value = $(cmp).val();

            for (var i = 0; i < validationRules.length; i++) {
                switch (validationRules[i].toLowerCase()) {
                    case 'required':
                        if (value == "") {
                            result = false;
                            message = message + 'Value is required\n';
                        }
                        break;

                    case 'email':
                        var emailfilter = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,4}$/;
                        if (!(emailfilter.test(value))) {
                            result = false;
                            message += 'Invalid Email \n';
                        }
                        break;

                    case 'date':
                          if (!moment(value,'YYYY-MM-DD').isValid()) {
                           var formatedDate= moment(value,'YYYY-MM-DD').format('dddd, MMMM Do YYYY, h:mm:ss a');
                            result = false;
                            message += "Invalid date of birth\n";
                        }
                      
                        break;
                }
            }
            return {
                message: message,
                result: result
            };


        }
        me.validate = function() {
            var result = true;
            var validationResult = null;
            /*  component = $("input[name='fName']");

              validationResult = me.validateComponent(['required']);
              if (!validationResult.result) {
                  me.showValidationErrror(validationResult.message);
                  result = validationResult.result;
              }

              component = $("input[name='Email']");
              validationResult = me.validateComponent(['required', 'email']);
              if (!validationResult.result) {
                  me.showValidationErrror(validationResult.message);
                  result = validationResult.result;
              }


              component = $("input[name='lName']");
              validationResult = me.validateComponent(['required']);
              if (!validationResult.result) {
                  me.showValidationErrror(validationResult.message);
                  result = validationResult.result;
              }



              component = $("input[name='dob']");
              validationResult = me.validateComponent(['required', 'date']);
              if (!validationResult.result) {
                  me.showValidationErrror(validationResult.message);
                  result = validationResult.result;
              }*/
            $('[data-validate]').each(function(index, cmp) {
                var validationRules = $(cmp).data('validate').split(",");
                validationResult = me.validateComponent(cmp, validationRules);
                if (!validationResult.result) {
                    me.showValidationErrror(cmp, validationResult.message);
                    result = validationResult.result;
                }
            })
            return result;


        }
      
     return me;

});

