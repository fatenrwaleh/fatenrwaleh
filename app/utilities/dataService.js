define(function() {
    
         var me = this;

         me.getIndexByID = function(gpatientId) {
             var result = null;
             for (var i = 0; i < patientsData.length; i++) {
                 if (patientsData[i].ID == gpatientId) {
                     result = i;
                     break;
                 }
             }
             return result;
         }

         me.getByID = function(gpatientId) {
             var index = me.getIndexByID(gpatientId);
             return patientsData[index]
         }

         me.removeByIndex = function(index) {
             var element = patientsData.splice(index, 1);
             return element;
         }

         me.removeById = function(gpatientId) {
             var index = me.getIndexByID(gpatientId);
             var element = me.removeByIndex(index);
             return element;
         }

         me.getPatientById = function(gpatientId) {
             var patient = null;
             for (var i = 0; i < patientsData.length; i++) {
                 if (patientsData[i].ID == gpatientId) {
                     patient = patientsData[i];

                     break;
                 }
             }
             return patient;
         }

         me.generateId = function() {
             var max = 0;
             var newId;
             for (var i = 0; i < patientsData.length; i++) {
                 if (patientsData[i].ID > max) {
                     max = patientsData[i].ID;
                 }
             }
             newId = max + 1;
             return newId;

         }


         return me;
     });
     


 