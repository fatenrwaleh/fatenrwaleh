/* global $ */

define( ['jquery','Utilities/routerEngine'],function($,routerEngine) {
        $(document).ready(function() {
                routerEngine
                        .State({
                                name:'patient-list',
                                screenId:'.patient-list',
                                url:'patientList',
                                html:'htmlCode/patient/patientlist.html',
                                controller:'app/Screens/listPatientScreen.js',
                                })
                        .State({
                                name:'patient-edit',
                                screenId:'.patient-edit',
                                url:'PatientEdit',
                                html:'htmlCode/patient/patientEdit.html',
                                controller:'app/Screens/editPatientScreen.js',
                        })
                        .State({
                                name:'user-list',
                                screenId:'.user-list',
                                url:'userlist',
                                html:'htmlCode/user/userlist.html',
                                controller:'',
                        })
                        .State({
                                name:'user-edit',
                                screenId:'.user-edit',
                                html:'htmlCode/user/useredit.html',
                                url:'useredit',
                                controller:''
                        })
              
    
               routerEngine.otherwise('patient-list');       
               routerEngine.inti();
               

                
        });    
});

/* $(document).ready(function() {
listPatientScreen.inti();
 editPatientScreen.inti();

 routerEngine.State({
         name:'patient-list',
         screenId:'.patient-list',
         url:'patientList',
         html:'htmlCode/patient/patientlist.html',
         controller:listPatientScreen,
      }).State({
         name:'patient-edit',
         screenId:'.patient-edit',
         url:'PatientEdit',
         html:'htmlCode/patient/patientEdit.html',
         controller:'',
      }).State({
         name:'user-list',
         screenId:'.user-list',
         url:'userlist',
         html:'htmlCode/user/userlist.html',
         controller:'',
      }).State({
         name:'user-edit',
         screenId:'.user-edit',
         html:'htmlCode/user/useredit.html',
         url:'useredit',
         controller:'',
      });
      
 routerEngine.otherwise('patient-list');       
 routerEngine.inti();
 });
 */
