/* global $ */

  define(['Utilities/routerEngine','Utilities/dataService','Libraries/moment','Utilities/ValidationEngine','Libraries/tostr/tostr'],function( dataService ,routerEngine, moment,ValidationEngine,tostr) {  
        var me = this;
        var gPatientId;
        var patientId;
        var formMode;
        me.emptyPatientForm = function() {
            $("input[name='fName']").val("");
            $("input[name='mName']").val("");
            $("input[name='lName']").val("");
            $("#status").val("");
            $("input[name='check']").val("");
            $("input[name='Email']").val("");
            $("input[name='dob']").val("");
            $("input[name='lCheck']").val("");
            $("input[name='genders']:checked").val("");

        }
        
        me.getFormData = function() {
            //collect patient data from form 
            var newID = dataService.generateId();
            var firstName = $("input[name='fName']").val();
            var middleName = $("input[name='mName']").val();
            var lastName = $("input[name='lName']").val();
            var Statuss = $("input[name='status']").val();
            var active = $("input[name='check']").val();
            var EMail = $("input[name='Email']").val();
            var DateOfBirth = $("input[name='dob']").val();
            var LastCheck = $("input[name='lCheck']").val();
            var Gender = $("input[name='genders']:checked").val();



            var patient = {
                ID: newID,
                fname: firstName,
                mname: middleName,
                lname: lastName,
                status: Statuss,
                Active: active,
                email: EMail,
                DOB: DateOfBirth,
                lastCheck: LastCheck,
                gender: Gender
            }
            return patient;
        }

        me.fillPatientForm = function(patient) {
            $("input[name='fName']").val(patient.fname);
            $("input[name='mName']").val(patient.mname);
            $("input[name='lName']").val(patient.lname);
            $("input[name='status']").val(patient.status);
            $("input[name='check']").val(patient.Active);
            $("input[name='Email']").val(patient.email);
            $("input[name='dob']").val(patient.DOB);
            $("input[name='lCheck']").val(patient.lastCheck);
            $("input[name='genders']").val(patient.gender);
        }
        /* 
          me.showAdd = function() {
            me.formMode = 'Add';
            routerEngine.go('patient-edit',true).then(function(){
                    me.emptyPatientForm;
                    
            });                  
          }

          me.showEdit = function(e) {
            me.formMode = 'edit';
           routerEngine.go('patient-edit',true).then(function(){
                 me.gpatientId = $(e.target).closest('tr').data('patientid');
                var patient = dataService.getPatientById(me.gpatientId);
                 me.fillPatientForm(patient);   

         });
          }  
      */
      me.init=function(routingData){
         debugger;
                 gpatientId=routingData.patientId;
                if (routingData.formMode != 'Add'){
                     var patient = dataService.getPatientById(routingData.patientId);
                     me.fillPatientForm(patient);
                     me.formMode = 'edit';

                }
                else{
                     me.formMode = 'Add';
                     me.emptyPatientForm();

                }

                $(" .savebutton").click(function(){
                     debugger;
                   tostr.options = {
                             "positionClass": "toast-top-right",
                             "fadeIn": 300,
                             "fadeOut": 1000,
                             "timeOut": 5000,
                             "extendedTimeOut": 1000
                   };
                   me.savePatient(gpatientId);
     });
            }
            


        
     /*   me.init = function() {
            $(".patient-list .addButton").onclick(function(){
                 me.showAdd();
            });
        } */ 

        me.render = function(template, data) {
            debugger;
            var template = document.getElementById('template').innerHTML;
            var data = {
                fname: 'faten',
                mname: 'M',
                lname: 'rwaleh',
                status: 'defult',
                Active: 'true',
                email: 'faten.rwaleh@gmail.com',
                DOB: '8/1/1992',
                lastCheck: '15/7/2017',
                gender: 'femal'
            }
            var results = [];
            var text;
            var reg = /{([^{}]*)}/g;
            for (var i in data) {
                if (text = reg.exec(template)) {
                    results.push(text[1]);
                    template = template.replace(text[1], data[i])
                }

            }
            return template;
        }

      

        me.getDates = function(startDate, endDate) {
            var startDate = '2017-07-01';
            var endDate = '2016-07-01';
            var dateArray = [];
            var stopDate = moment(endDate);
            var currentdate = moment(startDate);
            while (currentdate > stopDate) {
                dateArray.push(currentdate.subtract(1, 'months').format('YYYY-MM-DD'));
            }

            return dateArray;
        }

        me.savePatient = function(gpatientId) {
            debugger;
            var patient = me.getFormData();
            if (!ValidationEngine.validate(patient)) {
                return
            }

            if (me.formMode == 'Add') {
                patientsData.push(patient);

                tostr.success("The patient was successfully added");
            }
            else {
                
                var index = dataService.getIndexByID(gpatientId);
                patientsData[index] = patient;
                tostr.success("Patient data has been modified successfully");

            }
            routerEngine.go('patient-list',true);

        }

      //  me.removePatient = function(e) {
         //   var patient = dataService.removeById(me.gpatientId);
          //  listPatientScreen.inti();
       // }


        return me;

   
     
});