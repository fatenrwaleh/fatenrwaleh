"use strict";
/*global main*/
define(['jquery','Libraries/moment','Utilities/routerEngine','Utilities/dataService'],function($,moment,routerEngine,dataService) { 
  var patientId;
  function renderTable() {
    $('.tableInfo').empty();
    
    for (var i = 0; i < patientsData.length; i++) {
     var html = "<tr class='patient-row' data-patientid='" + patientsData[i].ID + "'>" +
      "<td>" + patientsData[i].ID + "</td>" +
      "<td>" + patientsData[i].fname + " " + patientsData[i].mname + " " + patientsData[i].lname + "</td>" +
      "<td>" + patientsData[i].email + "</td>" +
      "<td>" + patientsData[i].gender + "</td>" +
      "<td>" +  moment(patientsData[i].DOB).format('ddd, MMM DD YYYY, h:mm:ss [GMT]zZZ [(Jordan Standard Time)]')+ "</td>" +
      "<td>" + patientsData[i].Active + "</td>" +
      "<td>" + "by  " + patientsData[i].CreatedBy + "at  " + patientsData[i].creationDate + "</td>" +
      "<td> <button class='btn btn-primary route editbutton'  data-sref='patient-edit'  >Edit</td>" +
      "<td> <button class='btn btn-primary deletebutton '   >Delete</td>" +
      "</tr>";
     $('.tableInfo').append(html);

     
    }

  }
  

  function init()  {
      renderTable();
      $(".patient-list .addButton ").click(function(){
                     routerEngine.go('patient-edit',true,{formMode:'Add',patientId:null});

      });

      $(" .editbutton").click(function(e){
                       patientId = $(e.target).closest('tr').data('patientid');
                     routerEngine.go('patient-edit',true,{formMode:'edit',patientId:patientId});

     });

      $(" .deletebutton").click(function(e){
                     patientId = $(e.target).closest('tr').data('patientid');
                    var patient = dataService.removeById(patientId);
                   renderTable();

                     
     });

     
                
  }
                
  



  

  return {
    init:init,
  }
  });
